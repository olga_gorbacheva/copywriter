import java.io.*;
import java.util.ArrayList;

/**
 * Класс, позволяющий преобразовать повторяющиеся символы в точки
 *
 *
 * @author Горбачева, 16ИТ18к
 */
public class CopyWriter {
    public static void main(String[] args) {
        ArrayList<char[]> arrayList = getChars();
        if (arrayList.isEmpty()){
            System.out.println("Проверьте содержимое исходного файла");
        } else {
            rewrite(arrayList);
        }
    }

    /**
     * Возвращает список прочитанных строк из файла, представленных в виде массивов символов
     *
     * @return ArrayList массивов символов
     */
    private static ArrayList<char[]> getChars() {
        ArrayList<char[]> arrayList = new ArrayList<>();
        try (BufferedReader bufferedReader = new BufferedReader(new FileReader("input.txt"))) {
            String string;
            while ((string = bufferedReader.readLine()) != null) {
                if (string.contains(".")){
                    arrayList = new ArrayList<>();
                    break;
                } else {
                    arrayList.add(string.toCharArray());
                }
            }
        } catch (IOException e) {
            System.out.println("Произошла ошибка при чтении исходного файла");
        }
        return arrayList;
    }

    /**
     * Метод, выполняющий перезапись исходных строк с соответствующей заменой
     *
     * @param arrayList - исходные данные
     */
    private static void rewrite(ArrayList<char[]> arrayList) {
        try (BufferedWriter bufferedWriter = new BufferedWriter(new FileWriter("output.txt"))) {
            bufferedWriter.write(arrayList.get(0));
            bufferedWriter.write("\n");
            char ch = '.';
            for (int i = 1; i < arrayList.size(); i++) {
                char[] array1 = arrayList.get(i - 1);
                char[] array2 = arrayList.get(i);
                for (int j = 0; j < Math.min(array1.length, array2.length); j++) {
                    if ((array1[j] == array2[j]) && (ch == '.')) {
                        bufferedWriter.write('.');
                        ch = '.';
                    } else {
                        bufferedWriter.write(array2[j]);
                        ch = array2[j];
                    }
                }
                bufferedWriter.write("\n");
                ch = '.';
            }
        } catch (IOException e) {
            System.out.println("Произошла ошибка при записи данных в файл");
        }
    }
}
